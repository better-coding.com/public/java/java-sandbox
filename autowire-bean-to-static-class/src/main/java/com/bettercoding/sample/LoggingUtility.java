package com.bettercoding.sample;

public final class LoggingUtility {
    private static MyLogger myLogger;
    private static String infoPrefix;

    public static void setMyLogger(MyLogger myLogger) {
        LoggingUtility.myLogger = myLogger;
    }

    public static void setInfoPrefix(String infoPrefix) {
        LoggingUtility.infoPrefix = infoPrefix;
    }

    public static void info(String msg) {
        myLogger.log(infoPrefix + msg);
    }
}
