package com.bettercoding.sample;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MyLogger {
    @Value("${logger-prefix}")
    private String loggerPrefix;

    public void log(String msg) {
        System.out.printf("%s%s\n", loggerPrefix, msg);
    }
}
