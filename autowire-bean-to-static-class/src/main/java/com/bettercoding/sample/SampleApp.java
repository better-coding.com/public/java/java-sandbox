package com.bettercoding.sample;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleApp implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(SampleApp.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        LoggingUtility.info("Hello world :)");
    }
}
