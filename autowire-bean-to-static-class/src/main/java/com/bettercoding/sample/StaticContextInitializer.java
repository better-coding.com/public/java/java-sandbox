package com.bettercoding.sample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class StaticContextInitializer {
    @Autowired
    private MyLogger myLogger;

    @Value("${logger-info-prefix}")
    private String infoPrefix;

    @PostConstruct
    public void init() {
        LoggingUtility.setInfoPrefix(infoPrefix);
        LoggingUtility.setMyLogger(myLogger);
    }
}
