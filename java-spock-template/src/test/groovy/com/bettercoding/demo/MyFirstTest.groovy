package com.bettercoding.demo

import spock.lang.Specification

class MyFirstTest extends Specification {

  def "My First test"() {
    when:
      def dummy = 1
    then:
      dummy == 1
  }
}
