package com.bettercoding.h2databasecleanup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class H2DatabaseCleanupApplication {

    public static void main(String[] args) {
        SpringApplication.run(H2DatabaseCleanupApplication.class, args);
    }

}
