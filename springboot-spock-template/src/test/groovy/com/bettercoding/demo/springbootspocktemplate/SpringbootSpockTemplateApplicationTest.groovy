package com.bettercoding.demo.springbootspocktemplate

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.ApplicationContext
import spock.lang.Specification

@SpringBootTest
class SpringbootSpockTemplateApplicationTest extends Specification {

  @Autowired
  private ApplicationContext applicationContext

  def "My first Spock SpringBoot test"() {
    expect:
      applicationContext != null
  }
}
