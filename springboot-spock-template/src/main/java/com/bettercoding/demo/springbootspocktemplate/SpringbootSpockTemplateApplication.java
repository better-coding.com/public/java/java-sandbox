package com.bettercoding.demo.springbootspocktemplate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootSpockTemplateApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootSpockTemplateApplication.class, args);
	}

}
