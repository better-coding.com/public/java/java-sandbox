package com.bettercoding.timetraveling.timetravelingdemo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TimeConfiguration {
    @Bean
    DateTimeProvider dateTimeProvider() {
        return DateTimeProvider.getInstance();
    }
}
