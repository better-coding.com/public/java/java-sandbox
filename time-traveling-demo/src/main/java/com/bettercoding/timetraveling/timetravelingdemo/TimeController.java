package com.bettercoding.timetraveling.timetravelingdemo;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.ZonedDateTime;

@RestController
@RequestMapping("/time")
@RequiredArgsConstructor
public class TimeController {
    private final BigBen bigBen;

    @GetMapping("/")
    public ZonedDateTime getCurrentTime() {
        return bigBen.getApplicationTime();
    }
}
