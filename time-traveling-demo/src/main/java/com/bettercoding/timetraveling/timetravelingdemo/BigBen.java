package com.bettercoding.timetraveling.timetravelingdemo;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;

@Component
@RequiredArgsConstructor
public class BigBen {
    private final DateTimeProvider dateTimeProvider;

    public ZonedDateTime getApplicationTime() {
        return dateTimeProvider.timeNow();
    }
}
