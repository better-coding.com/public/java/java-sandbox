package com.bettercoding.timetraveling.timetravelingdemo;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.*;

import java.time.ZonedDateTime;

@RestController
@RequestMapping("/time-management")
@ConditionalOnProperty(prefix = "time-management", name = "enabled", havingValue = "true")
@RequiredArgsConstructor
public class TimeManagementController {
    private final DateTimeProvider dateTimeProvider;

    @PostMapping("/")
    public void changeApplicationTime(@RequestBody ChangeApplicationTimeRequest changeApplicationTimeRequest) {
        this.dateTimeProvider.setTime(changeApplicationTimeRequest.getZonedDateTime());
    }

    @DeleteMapping
    public void resetApplicationTime() {
        this.dateTimeProvider.resetTime();
    }

    @Data
    private static class ChangeApplicationTimeRequest {
        ZonedDateTime zonedDateTime;
    }
}
