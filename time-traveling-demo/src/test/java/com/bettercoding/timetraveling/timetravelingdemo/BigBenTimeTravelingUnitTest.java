package com.bettercoding.timetraveling.timetravelingdemo;


import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class BigBenTimeTravelingUnitTest {

    @BeforeAll
    static void setup() {
        ZonedDateTime customAppTime = ZonedDateTime.parse("1990-01-02T11:00:56+02:00", DateTimeFormatter.ISO_ZONED_DATE_TIME);
        DateTimeProvider.getInstance().setTime(customAppTime);
    }

    @AfterAll
    static void cleanup() {
        DateTimeProvider.getInstance().resetTime();
    }

    @Test
    void timeTravelingTest() {
        //given
        ZonedDateTime expectedTime = ZonedDateTime.parse("1990-01-02T11:00:56+02:00", DateTimeFormatter.ISO_ZONED_DATE_TIME);
        BigBen bigBen = new BigBen(DateTimeProvider.getInstance());
        //when
        var applicationTime = bigBen.getApplicationTime();
        //then
        Assertions.assertEquals(expectedTime, applicationTime);
    }
}
