package com.bettercoding.timetraveling.timetravelingdemo;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@SpringBootTest
class TimeTravelingDemoApplicationTests {
	@Autowired
	private BigBen bigBen;

	@BeforeAll
	static void setup() {
		ZonedDateTime customAppTime = ZonedDateTime.parse("1990-01-02T11:00:56+02:00", DateTimeFormatter.ISO_ZONED_DATE_TIME);
		DateTimeProvider.getInstance().setTime(customAppTime);
	}

	@AfterAll
	static void cleanup() {
		DateTimeProvider.getInstance().resetTime();
	}

	@Test
	void contextLoads() {
	}

	@Test
	void timeTravelTest() {
		//given
		ZonedDateTime expectedTime = ZonedDateTime.parse("1990-01-02T11:00:56+02:00", DateTimeFormatter.ISO_ZONED_DATE_TIME);
		//when
		ZonedDateTime applicationTime = bigBen.getApplicationTime();
		//then
		Assertions.assertEquals(expectedTime, applicationTime);
	}

}
