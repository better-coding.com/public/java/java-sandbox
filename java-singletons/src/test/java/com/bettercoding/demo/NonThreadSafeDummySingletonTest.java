package com.bettercoding.demo;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import static org.junit.jupiter.api.Assertions.*;

class NonThreadSafeDummySingletonTest {
    @Test
    void doubleInstanceCreatedTest() {
        //given
        CompletableFuture<Void> c1 = CompletableFuture.supplyAsync(() -> {
            NonThreadSafeDummySingleton.getInstance();
            return null;
        });

        CompletableFuture<Void> c2 = CompletableFuture.supplyAsync(() -> {
            NonThreadSafeDummySingleton.getInstance();
            return null;
        });

        List<CompletableFuture> all = List.of(c1, c2);
        //when
        all.forEach(CompletableFuture::join);
        //then
        assertEquals(2, NonThreadSafeDummySingleton.TOTAL_INSTANCES.get());
    }
}