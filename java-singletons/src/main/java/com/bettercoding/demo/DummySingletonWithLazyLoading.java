package com.bettercoding.demo;

public final class DummySingletonWithLazyLoading {
    private static DummySingletonWithLazyLoading instance = null;

    public static DummySingletonWithLazyLoading getInstance() {
        if (instance  == null ) {
            synchronized (DummySingletonWithLazyLoading.class) {
                if (instance  == null ) {
                    instance = initializeSingletonInstance();
                }
            }
        }
        return instance;
    }

    private DummySingletonWithLazyLoading() {
    }

    private static DummySingletonWithLazyLoading initializeSingletonInstance() {
        return new DummySingletonWithLazyLoading();
    }

    //This is dummy singleton's field
    private String dummyField = "Hello World!";

    //This is dummy singleton's method
    public void dummyMethod(String stringParam) {
        this.dummyField = stringParam;
    }

    //This is dummy singleton's method
    public String getDummyField() {
        return this.dummyField;
    }
}
