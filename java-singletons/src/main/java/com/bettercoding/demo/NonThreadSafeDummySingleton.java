package com.bettercoding.demo;

import java.util.concurrent.atomic.AtomicInteger;

public class NonThreadSafeDummySingleton {
    public static final AtomicInteger TOTAL_INSTANCES = new AtomicInteger();
    private static NonThreadSafeDummySingleton instance;

    public static NonThreadSafeDummySingleton getInstance() {
        if (instance == null) {
            instance = new NonThreadSafeDummySingleton();
        }
        return instance;
    }

    public NonThreadSafeDummySingleton() {
        final int currentInstance = TOTAL_INSTANCES.incrementAndGet();
        System.out.println("Creating NonThreadSafeDummySingleton instance number: " + currentInstance);
        if (currentInstance == 1) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        System.out.println("NonThreadSafeDummySingleton instance number: " + currentInstance + " created");
    }
}
