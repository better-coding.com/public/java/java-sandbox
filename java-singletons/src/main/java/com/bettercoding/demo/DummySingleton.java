package com.bettercoding.demo;

public enum DummySingleton {
    INSTANCE;

    public static DummySingleton getInstance() {
        return INSTANCE;
    }

    //This is dummy singleton's field
    private String dummyField = "Hello World!";

    //This is dummy singleton's method
    public void dummyMethod(String stringParam) {
        this.dummyField = stringParam;
    }

    //This is dummy singleton's method
    public String getDummyField() {
        return this.dummyField;
    }
}