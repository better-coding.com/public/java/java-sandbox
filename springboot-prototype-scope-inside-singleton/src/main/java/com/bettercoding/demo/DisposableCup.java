package com.bettercoding.demo;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;

@Component
@Scope(scopeName = "prototype")
public class DisposableCup {
    private static final AtomicInteger TOTAL_CUPS = new AtomicInteger();
    private final String name;

    public DisposableCup() {
        this.name = "Disposable cup number " + TOTAL_CUPS.incrementAndGet();
    }

    public String getName() {
        return name;
    }
}
