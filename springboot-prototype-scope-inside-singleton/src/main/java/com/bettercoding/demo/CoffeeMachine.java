package com.bettercoding.demo;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CoffeeMachine {
    @Autowired
    public ObjectFactory<DisposableCup> disposableCupObjectFactory;

    public DisposableCup brewCoffee() {
        DisposableCup aCup = disposableCupObjectFactory.getObject();
        System.out.printf("Brewing coffee in [%s]\n", aCup.getName());
        return aCup;
    }
}
